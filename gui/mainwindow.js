const electron = require('electron');
const simpleGit = require('simple-git');
const os = require('os');
const {ipcRenderer, remote, clipboard} = electron;
const {Menu, MenuItem} = remote;
const Manga = require('./../miscellaneous/manga');
const Library = require('./library');
const Utilities = require('./../miscellaneous/utilities');

//Load content into HTML
MangaLibrary = new Library("library.json", document)
MangaLibrary.load()
MangaLibrary.showLeft(MangaLibrary.getLeftList())
MangaLibrary.showRight(MangaLibrary.getRightList())
MangaLibrary.showStats()

//Creating context menus
const mangaMenu = new Menu()
const pendingMenu = new Menu()
const deleteMenu = new Menu()
var selectItemID = ""
// mangaMenu.append(new MenuItem({label: 'Add Entry', click() {
//     ipcRenderer.send('create:addWindow', "")
// }}))
// mangaMenu.append(new MenuItem({label: 'Update Entry', click() {
    //     let selection = MangaLibrary.getLeftSelection()
    //     let list = MangaLibrary.getLeftList()
    //     if (selection != ''){
        //         let index = Utilities.findById(list, selection)
        //         ipcRenderer.send('create:editWindow', list[index])
        //     }
        
        // }}))

mangaMenu.append(new MenuItem({label: 'Save', click() {
    MangaLibrary.save()
}}))
mangaMenu.append(new MenuItem({label: 'Copy Title', click() {
    let selection = MangaLibrary.getLeftSelection()
    let list = MangaLibrary.getLeftList()
    if (selection != ''){
        let index = Utilities.findById(list, selection)
        clipboard.writeText(list[index].title)
    }
    
}}))
mangaMenu.append(new MenuItem({type: 'separator'}))
mangaMenu.append(new MenuItem({label: 'generate "unchanged" report', click() {
    MangaLibrary.SaveUnchangedToFile()
    MangaLibrary.SaveUnknownToFile()
}}))
mangaMenu.append(new MenuItem({type: 'separator'}))
mangaMenu.append(new MenuItem({id: "toggleDelete", type: 'checkbox', label: 'DELETE MODE', click() {
    if (mangaMenu.getMenuItemById("toggleDelete").checked){
        MangaLibrary.enableDeleteMode()       
    }
    else{
        MangaLibrary.disableDeleteMode()
    }
}}))

pendingMenu.append(new MenuItem({label: 'Copy Link', click() {
    let index = Utilities.findById(MangaLibrary.getRightList(), MangaLibrary.getRightSelection())
    clipboard.writeText(MangaLibrary.getRightList()[index].link)
}}))
pendingMenu.append(new MenuItem({type: 'separator'}))
pendingMenu.append(new MenuItem({label: 'Delete Entry', click() {
    let index = Utilities.findById(MangaLibrary.getRightList(),MangaLibrary.getRightSelection())
    MangaLibrary.removeRight(index)
    document.getElementById(MangaLibrary.getRightSelection()).remove()
}}))

deleteMenu.append(new MenuItem({label: 'Empty Trash', click() {
    MangaLibrary.emptyTrashCan()
}}))

document.addEventListener('dragover', event => event.preventDefault());
document.addEventListener('drop', event => event.preventDefault());

// ============================================================================================
// ----------------- maintain layout during maximize/resize------------------------------------
ipcRenderer.on('resize', (e, windowHeight) => {
    newHeight = parseInt(windowHeight) - 50
    document.getElementById('secondRow').style.height = newHeight.toString() + 'px'
})
ipcRenderer.on('maximize', (e, windowHeight) => {
    newHeight = parseInt(windowHeight) - 66
    document.getElementById('secondRow').style.height = newHeight.toString() + 'px'
})
// ============================================================================================

document.getElementById('minimizeButton').addEventListener('click', (event) => {
    remote.getCurrentWindow().minimize(); 
})
document.getElementById('maximizeButton').addEventListener('click', (event) => {
    let window = remote.getCurrentWindow();
    if (window.isMaximized()){ window.unmaximize() }
    else{ window.maximize() }
})
document.getElementById('exitButton').addEventListener('click', (event) => {
    remote.getCurrentWindow().close(); 
})
document.getElementById('pushGitButton').addEventListener('click', (event) => {
    simpleGit().outputHandler((command, stdout, stderr) => {
        stdout.on('data', (data) => {
            // Print data
            document.getElementById("gitInfoField").innerText = data.toString('utf8').replace(/\n/g, ' ')
            console.log(data.toString('utf8'))
        })
     })
     .add('.')
     .commit(os.hostname())
     .push(['origin', 'master'])
})
document.getElementById('pullGitButton').addEventListener('click', (event) => {
    simpleGit().outputHandler((command, stdout, stderr) => {
        stdout.on('data', (data) => {
            // Print data
            document.getElementById("gitInfoField").innerText = data.toString('utf8').replace(/\n/g, ' ')
            console.log(data.toString('utf8'))
        })
    })
    .pull(['origin', 'master'], (error) => {
        MangaLibrary.reload()
    })
})

//Add item to left list from addWindow
ipcRenderer.on('manga:add', (e, arrayItems) => {
    if (arrayItems[0]){
        let newManga = new Manga(arrayItems[0], arrayItems[1], arrayItems[2], arrayItems[3], arrayItems[4], arrayItems[5], Date.now())
        MangaLibrary.addLeft(newManga)
    }
})
//Update item from left list
ipcRenderer.on('manga:update', (event, id, arrayItems) => {
    if (arrayItems[0]){
        MangaLibrary.updateLeftList(id, arrayItems)
    }
})
document.getElementById('MangaColumn').addEventListener('contextmenu', (event) => {
    event.preventDefault()
    if (event.target.id.substring(0, 6) == 'manga-'){
        MangaLibrary.setLeftSelection(event.target.id)
    }else{
        MangaLibrary.setLeftSelection('')
    }
    mangaMenu.popup({window: remote.getCurrentWindow()})
})

document.getElementById('Mangalist').addEventListener('dblclick', (event) => {
    let list = MangaLibrary.getLeftList()
    selectItemID = event.target.id
    let selectedIndex = Utilities.findById(list, event.target.id)
    let item = list[selectedIndex]
    let editWindowModal = document.getElementById('editWindowModal')

    editWindowModal.querySelector('#title').value = item.title
    editWindowModal.querySelector('#volume').value = item.volume
    editWindowModal.querySelector('#chapter').value = item.chapter
    editWindowModal.querySelector('#status').value = item.status
    editWindowModal.querySelector('#author').value = item.author
    editWindowModal.querySelector('#notes').value = item.notes

    document.getElementById("editWindowModal").style.display = "block";
    
});
document.getElementById('formEditItem').addEventListener('submit', (event) => {
    event.preventDefault();
    let editWindowModal = document.getElementById('editWindowModal')
    const title = editWindowModal.querySelector('#title').value.trim();
    const volume = editWindowModal.querySelector('#volume').value;
    const chapter = editWindowModal.querySelector('#chapter').value;
    const status = editWindowModal.querySelector('#status').value;
    const author = editWindowModal.querySelector('#author').value.trim();
    const notes = editWindowModal.querySelector('#notes').value;
    
    ipcRenderer.send('manga:update', selectItemID, [title, volume, chapter, status, author, notes]);

    document.getElementById("editWindowModal").style.display = "none";
});

document.getElementById('Mangalist').addEventListener('click', (event) => {
    if (MangaLibrary.getMode()){
        MangaLibrary.moveToTrash(event)
    }
})
function filterLeftList(text, event){
    //filter list
    //to avoid rerendering the whole list each time user press backspace or delete on already empty input box, every other function key will rerender it though
    let specialKeys = event.key != 'Backspace' && event.key != 'Delete'
    let properLength  = MangaLibrary.getLeftListLength() != document.getElementById('Mangalist').children.length
    if (specialKeys || text != "" || properLength){
        MangaLibrary.filterLeft(document.getElementById("mangaInput").value)
    }
}
document.getElementById('mangaInput').addEventListener('contextmenu', (event) => {
    document.execCommand('paste')
    let text = document.getElementById("mangaInput").value
    filterLeftList(text, event)
})
document.getElementById("mangaInput").addEventListener('keyup', (event) => {
    let text = document.getElementById("mangaInput").value
    filterLeftList(text, event)
    //open addWindow for entering new manga
    if(event.key == "Enter" && text != ""){
        if (MangaLibrary.initiateAddManga(text)){
            document.getElementById("addWindowModal").style.display = "block";
            document.querySelector('#title').value = text
        }
    }
})
document.getElementById('formAddNewItem').addEventListener('submit', (event) => {
    event.preventDefault();
    let addWindowModal = document.getElementById("addWindowModal")
    const title = addWindowModal.querySelector('#title').value.trim();
    const volume = addWindowModal.querySelector('#volume').value;
    const chapter = addWindowModal.querySelector('#chapter').value;
    const status = addWindowModal.querySelector('#status').value;
    const author = addWindowModal.querySelector('#author').value.trim();
    const notes = addWindowModal.querySelector('#notes').value;
    ipcRenderer.send('manga:add', [title, volume, chapter, status, author, notes]);
    document.getElementById("addWindowModal").style.display = "none";
    addWindowModal.querySelector('#title').value = ""
    addWindowModal.querySelector('#volume').value = ""
    addWindowModal.querySelector('#chapter').value = ""
    addWindowModal.querySelector('#author').value = ""
    addWindowModal.querySelector('#notes').value = ""
});

document.getElementById('Pendinglist').addEventListener('contextmenu', (event) => {
    if (event.target.id.substring(0, 8) == 'pending-'){
        MangaLibrary.setRightSelection(event.target.id)
        pendingMenu.popup({window: remote.getCurrentWindow()})
    }
})
document.getElementById('Pendinglist').addEventListener('dblclick', (event) => {
    if (event.target.id.substring(0, 8) == 'pending-'){
        let index = Utilities.findById(MangaLibrary.getRightList(), event.target.id)
        clipboard.writeText(MangaLibrary.getRightList()[index].link)
    }
})
//add item to the right list
document.getElementById("pendingInput").addEventListener('keyup', (event) => {
    let text = MangaLibrary.document.getElementById("pendingInput").value
    if (event.key == "Enter" && text != ""){
        MangaLibrary.addRight(text)
    }
})

document.getElementById('rightColumnDeleteMode').addEventListener('contextmenu', (event) => {
    deleteMenu.popup({window: remote.getCurrentWindow()})
})

document.getElementById('Trashlist').addEventListener('click', (event) => {
    MangaLibrary.moveFromTrash(event)
})

//Save library
window.addEventListener('keydown', (event) => {
    if (event.key.toLowerCase() == 's' && event.ctrlKey){
        MangaLibrary.save()
    }else if (event.key == 'Escape'){
        document.getElementById("addWindowModal").style.display = "none";
        document.getElementById("editWindowModal").style.display = "none";
    }
})

window.onbeforeunload = (event) => {
    if (MangaLibrary.getSaveStatus() == 'visible')
    {
        if (confirm('There are unsaved changes, do you wish to save before closing the application?')){
            MangaLibrary.save()
        }
    }
}
window.onclick = function(event) {
    if (event.target == document.getElementById("addWindowModal")) {
        document.getElementById("addWindowModal").style.display = "none";
    }else if (event.target == document.getElementById("editWindowModal")) {
        document.getElementById("editWindowModal").style.display = "none";
    }
}