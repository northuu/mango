const JsonReader = require('./miscellaneous/jsonReader')


let data = JsonReader.open("./library.json")
let newData = {lib: [], pending : []}

newData.pending = data.pending

data.lib.forEach((element) => {
    element['last_changed'] = Date.now()
    newData.lib.push(element)
})

JsonReader.save("./library.json", newData)